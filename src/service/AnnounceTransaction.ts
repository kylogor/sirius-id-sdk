import {Account, NetworkType, TransactionHttp, Listener, BlockHttp, NetworkHttp, SignedTransaction,
    Address,Transaction,TransactionType, LockFundsTransaction, Deadline, Mosaic, MosaicId,
    UInt64,
    NamespaceId,
    AccountHttp,
    PublicAccount,
    QueryParams,
    AggregateTransaction,
    CosignatureTransaction} from 'tsjs-xpx-chain-sdk';


export class AnnounceTransaction{

    static lastId: string;

    constructor(){}

    static async announce(transaction:any,privateKey:string,apiNode:string,networkType = NetworkType.TEST_NET){
        const transactionHttp = new TransactionHttp(apiNode);

        const wsAddress = apiNode.replace('https', 'wss').replace('http', 'ws');
        const listener = new Listener(wsAddress, WebSocket);

        const sender = Account.createFromPrivateKey(privateKey,networkType);

        const generationHash = await this.getGenerationHash(apiNode);

        const signedTx = sender.sign(transaction,generationHash);

        console.log("hash: " + signedTx.hash);

        
        let address;
        switch (transaction.type){
            case TransactionType.TRANSFER:
                console.log("transferrrrr");
                address = transaction.recipient;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.AGGREGATE_COMPLETE:
                console.log("completeddddd");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.AGGREGATE_BONDED:
                console.log("bondeddddd");
                address = sender.address;
                return await this.announceAggregateBonded(signedTx,listener,transactionHttp,address,sender,networkType,generationHash);
            case TransactionType.ADDRESS_ALIAS:
                console.log("address aliasssss");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.LINK_ACCOUNT:
                console.log("link accountttt");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.REGISTER_NAMESPACE:
                console.log("register namespaceeee");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.MOSAIC_ALIAS:
                console.log("mosaic aliassss");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.MOSAIC_DEFINITION:
                console.log("mosaic definitionnnn");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.MOSAIC_SUPPLY_CHANGE:
                console.log("mosaic supply changeeee");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.MODIFY_MOSAIC_METADATA:
                console.log("modify mosaic metadataaaa");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.MODIFY_NAMESPACE_METADATA:
                console.log("modify namespace metadataaaa");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.MODIFY_ACCOUNT_METADATA:
                console.log("modify account metadataaa");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.MODIFY_CONTRACT:
                console.log("modify contractttt");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.MODIFY_ACCOUNT_RESTRICTION_ADDRESS:
                console.log("modify account property addresssss");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.MODIFY_ACCOUNT_RESTRICTION_MOSAIC:
                console.log("modify account property mosaicccc");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.MODIFY_ACCOUNT_RESTRICTION_OPERATION:
                console.log("modify account property entityyyy");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.SECRET_LOCK:
                console.log("secret lockkk");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.SECRET_PROOF:
                console.log("secret prooffff");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.CHAIN_CONFIGURE:
                console.log("blockchain configggg");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
            case TransactionType.CHAIN_UPGRADE:
                console.log("blockchain upgradeeee");
                address = sender.address;
                return await this.announceTransfer(signedTx,listener,transactionHttp,address);
        }

        
    }

    static async announceTransfer(signedTx:SignedTransaction,listener:Listener,transactionHttp:TransactionHttp,address:Address){
        return new Promise(resolve => {
            let checkTimeout = setTimeout(() => {
                console.log("da qua 1 phut, check thu trang thai cua ma hash: " + signedTx.hash);
                transactionHttp.getTransactionStatus(signedTx.hash).subscribe(result => {
                    console.log("co ket qua cua ma hash: " + signedTx.hash);
                    console.log(result);
                    if (result.status != "Success"){
                        clearTimeout(checkTimeout);
                        resolve(false);
                    }
                },error => {
                    resolve(false);
                }, () => {
                    console.log("donne");
                }) 
            },60000);

            listener.open().then(() => {
                const subscription = listener.confirmed(address).subscribe(confirmed => {
                    if (confirmed && confirmed.transactionInfo && confirmed.transactionInfo.hash === signedTx.hash) {
                        console.log('confirmed: ' + JSON.stringify(confirmed));
                        resolve(true);
                        subscription.unsubscribe();
                        subscription2.unsubscribe();
                        listener.close();
                    }
                }, error => {
                    resolve(false);
                    console.error(error);
                }, () => {
                    console.log('done.');
                });

                const subscription2 = listener.status(address).subscribe(status => {//kiem tra loi tra ve neu co
                    if (status && status.hash === signedTx.hash) {
                        console.log('status: ' + JSON.stringify(status));
                        resolve(false);
                        subscription2.unsubscribe();
                        subscription.unsubscribe();
                        listener.close();
                    }
                }, error => {
                console.error(error);
                }, () => {
                    console.log('done2.');
                });

                transactionHttp.announce(signedTx);
            });
        })
    }

    static async announceAggregateBonded(signedTx:SignedTransaction,listener:Listener,transactionHttp:TransactionHttp,address:Address,sender:Account,networkType: NetworkType,generationHash:any){
        const lockFunds = LockFundsTransaction.create(
            Deadline.create(),
            new Mosaic(new NamespaceId('prx.xpx'), UInt64.fromUint(10 * 1000000)),
            UInt64.fromUint(50),
            signedTx,
            networkType
        )
        
        const lockFundsSigned = sender.sign(lockFunds,generationHash);
        console.log("lockFunds Hash:", lockFundsSigned.hash);
        return new Promise(resolve => {
            let checkTimeout = setTimeout(() => {
                transactionHttp.getTransactionStatus(signedTx.hash).subscribe(result => {
                  console.log(result);  
                }, error => {
                    resolve(false);
                }, () => {
                    console.log("donne");
                }) 
            },60000);

            listener.open().then(() => {
                const lockFundsSub = listener.confirmed(address).subscribe(lockFundsConfirmed => {
                    if (lockFundsConfirmed && lockFundsConfirmed.transactionInfo && lockFundsConfirmed.transactionInfo.hash === lockFundsSigned.hash){
                        console.log('lockFuncdsConfirmed: ' + JSON.stringify(lockFundsConfirmed));
                        const subscription = listener.aggregateBondedAdded(address).subscribe(aggregateTx => {
                            console.log("co vao nha");
                            console.log(aggregateTx);
                            if (aggregateTx && aggregateTx.transactionInfo && aggregateTx.transactionInfo.hash === signedTx.hash) {
                                console.log('aggregateTx: ' + JSON.stringify(aggregateTx));
                                resolve(true);
                                subscription.unsubscribe();
                                lockFundsSub.unsubscribe();
                                listener.close();
                            }
                        }, error => {
                            resolve(false);
                            console.error(error);
                        }, () => {
                            console.log('done...');
                        });
            
                        transactionHttp.announceAggregateBonded(signedTx);
                        
                    }
                }, error => {
                    resolve(false);
                    console.error(error);
                }, () => {
                    console.log('lockFunds Transaction Confirmed');
                });

                transactionHttp.announce(lockFundsSigned);

            });
        })
    }

    static async announceCosignature(transactionHash:string,privateKey:string,apiNode:string,networkType = NetworkType.TEST_NET){
        const sender = Account.createFromPrivateKey(privateKey,networkType);

        let accountHttp = new AccountHttp(apiNode);

        let queryParams;
        let result = await this.getCosignatureTransaction(accountHttp,sender.publicAccount,queryParams,transactionHash);
        while (true){
            if (result instanceof AggregateTransaction){
                console.log("da tim thay");
                return this.sendCosignatureTransaction(result,privateKey,apiNode,networkType);
            }
            else if ((typeof result) == 'string'){
                console.log("chua tim thay");
                console.log(result);
                queryParams = new QueryParams(10,<string>result);
                result = await this.getCosignatureTransaction(accountHttp,sender.publicAccount,queryParams,transactionHash);
            }
            else return false;
        }
    }

    static async getCosignatureTransaction(accountHttp: AccountHttp, publicAccount: PublicAccount, queryParams:QueryParams | undefined, transactionHash:string ){
        console.log("getCosignatureTransaction");
        return new Promise(resolve => {
            accountHttp.aggregateBondedTransactions(publicAccount,queryParams).subscribe(tx => {
                console.log("all transactions");
                console.log(tx);
                console.log("hash: " + transactionHash);
                for (let i=0;i<tx.length;i++){
                    let transaction = tx[i];
                    if (transaction.transactionInfo && transaction.transactionInfo.hash == transactionHash){
                        console.log("da vao day roi ma");
                        resolve(transaction);
                        break;
                    }
                    else console.log("khong phai roi");
                }
                if (tx.length > 0){
                    let transactionLast = tx[tx.length-1];
                    if (transactionLast.transactionInfo){
                        resolve(transactionLast.transactionInfo.hash);
                    }
                }
                else resolve(null);
            }, error => {
                resolve(false);    
                console.log(error);
            })
        })
        
    }

    static async sendCosignatureTransaction(transaction:any,privateKey:string,apiNode:string,networkType:NetworkType){
        const transactionHttp = new TransactionHttp(apiNode);

        const wsAddress = apiNode.replace('https', 'wss').replace('http', 'ws');
        const listener = new Listener(wsAddress, WebSocket);

        const sender = Account.createFromPrivateKey(privateKey,networkType);

        const tx = CosignatureTransaction.create(transaction);
        const signedTx = sender.signCosignatureTransaction(tx);

        console.log("hash: " + signedTx.parentHash);

        return new Promise(resolve => {
            listener.open().then(() => {
                const subscription = listener.cosignatureAdded(sender.address).subscribe(cosinature => {
                    console.log("met r nha");
                    if (cosinature && cosinature.parentHash === signedTx.parentHash) {
                        console.log('cosinature: ' + JSON.stringify(cosinature));
                        resolve(true);
                        subscription.unsubscribe();
                        listener.close();
                    }
                }, error => {
                    resolve(false);
                    console.error(error);
                }, () => {
                    console.log('done...');
                });
    
                transactionHttp.announceAggregateBondedCosignature(signedTx);

            });
        })
    }
    static async getGenerationHash(apiNode:string){
        const networkHttp = new NetworkHttp(apiNode);
        const blockHttp = new BlockHttp(apiNode,networkHttp);
        return new Promise((resolve,rejects) => {
            blockHttp.getBlockByHeight(1).subscribe(blockInfo => {
                resolve(blockInfo.generationHash);
            }, error => {
                console.log(error);
            }, () => {
                console.log("Done");
            });
        })
    }
}