import {
    TransferTransaction,
    EncryptedMessage,
    PublicAccount,
    Account,
    NetworkType
} from 'tsjs-xpx-chain-sdk';
import { Credentials } from '../message/Credentials';
import { ApiNode } from './ApiNode';
export class VerifytoConfirmCredential{
    // Compare between sessionToken generate by dApp and encryptedSessionToken sent from SiriusID to verify
    success = false;
    static message: any;
    public static verify(transactionReceived: any, sessionToken: string, credential:Credentials,privateKey:string){
        const verify = new VerifytoConfirmCredential(transactionReceived, sessionToken,credential, privateKey);
        return verify.success;        
    }

    constructor (
        transaction:any,
        sessionToken: string,
        credential:Credentials,
        privateKey: string
    ){
        const transferTx = transaction as TransferTransaction;
        let dAppAccount = Account.createFromPrivateKey(privateKey,ApiNode.networkType);
        if (transferTx.message && transferTx.signer?.publicKey != dAppAccount.publicKey){ //don't listen tx was went from itself
            const transferTxMessage = EncryptedMessage.createFromPayload(transferTx.message.payload);
            const plainMessage = EncryptedMessage.decrypt(transferTxMessage,privateKey,<PublicAccount>transferTx.signer);
            //them buoc giai nen o dau day ne
            
            const JSONtransferTxmessage = JSON.parse(plainMessage.payload);
            VerifytoConfirmCredential.message = JSONtransferTxmessage;
            let credentialsUser = JSONtransferTxmessage.payload.credential;
            let step1 = Credentials.compareHashStr(credential.getContent(),credentialsUser.auth_owner,<string>transferTx.signer?.publicKey);
            if (step1 == true && sessionToken == JSONtransferTxmessage.payload.sessionToken){
                credential.addAuthOwner(credentialsUser.auth_owner);
                if (JSON.stringify(credential) == JSON.stringify(credentialsUser)){
                    this.success = true;
                }
                else this.success = false;
            }
            else this.success = false;
        }
        else this.success = false;
    }

    static getMessage(){
        return VerifytoConfirmCredential.message;
    }
}