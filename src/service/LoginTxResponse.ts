import { SiriusIDTransactionMessage } from '../message/SiriusIDTransactionMessage';
import { MessageReceived } from '../service/MessageReceived';
import { CompressData } from '../service/CompressData';
import {
    Account,
    TransactionHttp,
    Address,
    Listener,
    TransferTransaction,
    NetworkType,
    PlainMessage,
    Deadline,
    UInt64,
    BlockHttp,
    EncryptedMessage,
    PublicAccount,
} from 'tsjs-xpx-chain-sdk';
import { CredentialStored } from '../message/CredentialStored';

export class LoginTxResponse {
    public static successTx: boolean;
    
    //SiriusID announce a transfer transaction to dApp with response Login message after scan QRcode or get data from URL deeplink
    //The session Token is encrypted by function encrypt(sesstion);
    public static message(
        Sender: string,
        apiNode: string,
        networkType: NetworkType,
        scannedCode: MessageReceived,  
    ) 
    {
        // check form of api address 
        this.checkApiAddress(apiNode);
        const sender = Account.createFromPrivateKey(Sender, networkType)
        //const JSONscannedCode = JSON.parse(scannedCode)
        const recipient = Address.createFromPublicKey(scannedCode.message.payload.appPublicKey, networkType);
        const responseMessage = SiriusIDTransactionMessage.create(scannedCode.message.payload.sessionToken,scannedCode.message.payload.credentials,scannedCode.message.payload.addition);
        const wsAddress = apiNode.replace('https', 'wss').replace('http', 'ws');
        const listener = new Listener(wsAddress, WebSocket);
        const transactionHttp = new TransactionHttp(apiNode);

        const publicAccountRecipient = PublicAccount.createFromPublicKey(scannedCode.message.payload.appPublicKey, networkType);
        
        const encryptedMessage = EncryptedMessage.create(responseMessage.toString(),publicAccountRecipient,Sender);
        //transfer transaction contains data message 
        const tx = TransferTransaction.create(
            Deadline.create(),
            recipient,
            [],
            //PlainMessage.create(responseMessage.toString()),
            encryptedMessage,
            networkType,
            new UInt64([0,0])
        );
        return new Promise(resolve => {
            console.log('get network generation hash');
            const blockHttp = new BlockHttp(apiNode);
            // get network generation hash of node
            blockHttp.getBlockByHeight(1).subscribe(
                blockinfo => {
                    let networkgenegationHash = blockinfo.generationHash;
                    const signedTx = sender.sign(tx, networkgenegationHash);
                    console.log(signedTx.hash);

                    let checkTimeout = setTimeout(() => {
                        console.log("da qua 1 phut, check thu trang thai cua ma hash: " + signedTx.hash);
                        transactionHttp.getTransactionStatus(signedTx.hash).subscribe(result => {
                            console.log("co ket qua cua ma hash: " + signedTx.hash);
                            console.log(result);
                            if (result.status != "Success"){
                                clearTimeout(checkTimeout);
                                this.successTx = false;
                                resolve(this.successTx);
                            }
                        },error => {
                            resolve(false);
                        }, () => {
                            console.log("donne");
                        }) 
                    },60000);

                    listener.open().then(() => {
                        // listen confirm transaction from recipient
                        const subscription = listener.confirmed(recipient).subscribe(confirmed => {
                            if (confirmed && confirmed.transactionInfo && confirmed.transactionInfo.hash === signedTx.hash) {
                                console.log('confirmed: ' + JSON.stringify(confirmed));
                                this.successTx = true;
                                resolve(this.successTx);
                                subscription.unsubscribe();
                                listener.close();
                            }
                        }, error => {
                            this.successTx = false;
                            resolve(this.successTx);
                            console.error(error);
                        }, () => {
                            console.log('done.');
                        });
                        // announ transfer transaction
                        transactionHttp.announce(signedTx);
                    });
                })
            }
        )
    }   
    private static encrypt(sessionToken: string) {
        return sessionToken;
    }

    private static checkApiAddress(apiNode : string){
        const urlPattern = RegExp(/^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)$/igm);
            if (!urlPattern.test(apiNode)) throw new Error('Invalid_Api_Node_Address');
    }
}