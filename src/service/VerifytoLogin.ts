import {
    TransferTransaction,
    EncryptedMessage,
    PublicAccount
} from 'tsjs-xpx-chain-sdk';

import {CredentialStored} from '../message/CredentialStored';
import { AdditionStored } from '../message/AdditionStored';
export class VerifytoLogin{
    // Compare between sessionToken generate by dApp and encryptedSessionToken sent from SiriusID to verify
    success = false;
    static message: any;
    static credentials: any[];
    static addition:any[];

    static async verify(transactionReceived: any, sessionToken: string, credentials:string[],privateKey:string, addition ? :any){
        // let success = false;
        const transferTx = transactionReceived as TransferTransaction;
        // const transferTxMessage = transferTx.message.payload;
        const senderPublicKey = transferTx.signer?.publicKey;
        const transferTxMessage = EncryptedMessage.createFromPayload(transferTx.message.payload);
        const plainMessage = EncryptedMessage.decrypt(transferTxMessage,privateKey,<PublicAccount>transferTx.signer);
        
        const JSONtransferTxmessage = JSON.parse(plainMessage.payload);
        console.log(JSONtransferTxmessage);
        VerifytoLogin.message = JSONtransferTxmessage;
        let step1 = await this.verifyCredentials(sessionToken,JSONtransferTxmessage,senderPublicKey,credentials);
        let step2 = false;
        if (addition){
            step2 = await this.verifyAddition(JSONtransferTxmessage,addition);
        }
        else step2 = true;
        

        // let credentialsReceived = new Array();
        // for (let i=0;i<JSONtransferTxmessage.payload.credentials.length;i++){
        //     let el = new CredentialStored(JSONtransferTxmessage.payload.credentials[i]['keyDecrypt'],JSONtransferTxmessage.payload.credentials[i]['credentialHash']);
        //     console.log(el);
        //     let credential = await el.getCredential(el.credentialHash,el.keyDecrypt,<string>senderPublicKey);
        //     credentialsReceived.push(credential);
        // }
        // console.log("<<<<<<<<<<<");
        // console.log(credentialsReceived);
        // let credentials_req = this.intersecArray(credentialsReceived,credentials);
        // //Check if message received is a LOGIN message
        // if(JSONtransferTxmessage.messageType == 1){
        //     console.log('This is login response message');
        //     if (sessionToken == JSONtransferTxmessage.payload.encryptedSessionToken
        //         && credentials_req.length == credentials.length
        //     ){
        //         VerifytoLogin.credentials = credentialsReceived;
        //         console.log("Login successssss");
        //         success = true;
        //     }
        //     else{
        //         console.log("Login faileddddd");
        //     }
        // }
        if(step1 && step2){
            return true;
        }
        else return false;      
    }

    constructor (
        transaction:any,
        sessionToken: string,
        credentials:string[],
        privateKey: string
    ){}

    static intersecArray(arr1:any[], arr2:string[]){
        return arr1.filter(element1 => arr2.includes(element1['id']));
    }

    static getMessage(){
        return VerifytoLogin.message;
    }

    static async verifyCredentials(sessionToken:string, JSONtransferTxmessage:any,senderPublicKey:string | undefined,credentials:string[]){
        let credentialsReceived = new Array();
        for (let i=0;i<JSONtransferTxmessage.payload.credentials.length;i++){
            let el = new CredentialStored(JSONtransferTxmessage.payload.credentials[i]['keyDecrypt'],JSONtransferTxmessage.payload.credentials[i]['credentialHash']);
            console.log(el);
            let credential = await el.getCredential(el.credentialHash,el.keyDecrypt,<string>senderPublicKey);
            credentialsReceived.push(credential);
        }
        console.log("<<<<<<<<<<<");
        console.log(credentialsReceived);
        let credentials_req = this.intersecArray(credentialsReceived,credentials);
        //Check if message received is a LOGIN message
        if(JSONtransferTxmessage.messageType == 1){
            console.log('This is login response message');
            if (sessionToken == JSONtransferTxmessage.payload.encryptedSessionToken
                && credentials_req.length == credentials.length
            ){
                VerifytoLogin.credentials = credentialsReceived;
                console.log("Login successssss");
                return true;
            }
            else{
                console.log("Login faileddddd");
                return false;
            }
        }
    }

    static async verifyAddition(JSONtransferTxmessage:any,addition:any){
        let additionReceived = new Array();
        for (let i=0;i<JSONtransferTxmessage.payload.addition.length;i++){
            let el = new AdditionStored(JSONtransferTxmessage.payload.addition[i]['publicKey'],JSONtransferTxmessage.payload.addition[i]['privateKey'],JSONtransferTxmessage.payload.addition[i]['additionHash']);
            let addition = await el.getAddition();
            additionReceived.push(addition);
        }
        console.log("addition received--->");
        console.log(additionReceived);

        if(addition.length != additionReceived.length){
            return false;
        }
        
        for (let i=0;i<addition.length;i++){
            if (addition[i]['name'] != additionReceived[i]['name']){
                return false;
            }
        }
        VerifytoLogin.addition = additionReceived;
        return true;
    }
}