import { MessageType } from './MessageType';
import { AppMessage } from './AppMessage';
import { Credentials } from './Credentials';
export class CredentialConfirmMessage extends AppMessage {

    /**
     * Creation Date of message
     */
    createTimestamp: number;

    private token:string;

    /**
     * Create login request message
     * @param publicKey publicKey of dApp
     * 
     * @param credentials array id of credentials dApp need
     */
    public static create(publicKey: string,credential:Credentials) {
        const token = this.generateToken();
        return new CredentialConfirmMessage(publicKey, token, credential);
    }

    constructor(
        appPublicKey: string,
        sessionToken:string,
        credential:Credentials
    ){
        const type = MessageType.CREDENTIAL_REQ;
        const payload = {
            appPublicKey,
            sessionToken,
            credential
        }
        super(type, payload);

        this.createTimestamp = Date.now();
        this.token = payload.sessionToken;

    }

    getSessionToken(){
        return this.token;
    }

    /**
     * Generate random string of sesion token
     */
    private static generateToken() {
        
        let outString: string = '';
        let inOptions: string = 'abcdefghijklmnopqrstuvwxyz0123456789';

        for (let i = 0; i < 64; i++) {

            outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));

        }
        return outString;
    }
}