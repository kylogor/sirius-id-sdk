import { MessageType } from './MessageType';
import { AppMessage } from './AppMessage';
import { Credentials } from './Credentials';

export class CredentialRequestMessage extends AppMessage {

    constructor(credentials:Credentials){
        let payload = {
            credentials
        }
        let type = MessageType.CREDENTIAL;
        super(type, payload);
    }

    static create(credentials:Credentials){
        return new CredentialRequestMessage(credentials);
    }

}