import {MessageType} from './MessageType';
import {SiriusIdMessage} from './SiriusIdMessage';
import { Credentials } from './Credentials';
import { CredentialStored } from './CredentialStored';

export class SiriusIDTransactionMessage extends SiriusIdMessage{

    public static create(sessionToken: string,credentials:CredentialStored[],addition ? :object[]){
        return new SiriusIDTransactionMessage(this.encrypted(sessionToken),credentials,addition);
    }

    constructor(
        encryptedSessionToken: string,
        credentials: CredentialStored[],
        addition ? : object[]
    ){
        const messageType = MessageType.LOG_IN;
        const payload = {
            encryptedSessionToken,
            credentials,
            addition
        }
        super(messageType, payload);
    }

    public static encrypted(sessionToken: string){
        return sessionToken;
    }

    toString() {
        const message = {
            appID: this.appID,
            messageType: this.type,
            appVersion: this.version,
            payload: this.payload
        }
        return JSON.stringify(message);
    }
}