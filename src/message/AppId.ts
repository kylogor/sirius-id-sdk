export class AppId {
    /**
     * App Name
     */
    public static readonly APP_NAME = 'SiriusID';
    /**
     * Version of SiriusID App/SDK
     */
    public static readonly APP_VERSION = '0.0.1';
}