export class CredentialRequired{

    private constructor(
        public publicKeyEnc:string,
        public privateKeyEnc:string,
        public credentialHash: string,
    ){
    }

    static create(
        publicKeyEnc:string,
        privateKeyEnc:string,
        credentialHash: string,
    ){
        return new CredentialRequired(publicKeyEnc,privateKeyEnc,credentialHash);
    }

    getPublicKeyEnc(){
        return this.publicKeyEnc;
    }

    getPrivateKeyEnc(){
        return this.publicKeyEnc;
    }

    getCredentialHash(){
        return this.credentialHash;
    }

}