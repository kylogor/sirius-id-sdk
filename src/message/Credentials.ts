import {Md5} from 'ts-md5/dist/md5';
import { EncryptedMessage, PublicAccount, NetworkType } from 'tsjs-xpx-chain-sdk';
import {ApiNode} from '../service/ApiNode';
export class Credentials{
    private static globalPrivateKey = "27745A12D8EE237BE979543F76FDDEFE8B266273DDCB4BC23C31DBAF7214F548";
    private static globalPublicKey = "E0665BAB43304CF279F13C7EFB48A857DD03D5BB0372FDC7DAFE9DDAA01ECA5A";

    private constructor(
        private id:string,
        private name:string,
        private description:string,
        private icon:string,
        private documentHash:string[],
        private content:Array<[string,string]>,
        private group ? : string | null,
        private auth_origin ? : string | null,
        private auth_owner ? : string | null
    ){}

    static create(
        id:string,
        name:string,
        description:string,
        icon:string,
        documentHash: string[],
        content:Map<string,string>,
        group ? : string | null,
        auth_origin ? :string | null,
        auth_owner ? : string | null
    ){
        let content_arr = Array.from(content);
        if (!auth_origin){
            auth_origin = null;
        }
        if (!auth_owner){
            auth_owner = null;
        }
        return new Credentials(id,name,description,icon,documentHash,content_arr,group,auth_origin,auth_owner);
    }

    static authCreate(content:any, privateKey: string){
        let hashStr = Md5.hashStr(JSON.stringify(content));
        let pubAccount = PublicAccount.createFromPublicKey(this.globalPublicKey, ApiNode.networkType);
        return EncryptedMessage.create(<string>hashStr,pubAccount,privateKey).payload;
    }

    static compareHashStr(content:any,encryptedStr:string,publicKey:string){
        let hashStr1 = Md5.hashStr(JSON.stringify(content));
        let pubAccount = PublicAccount.createFromPublicKey(publicKey, ApiNode.networkType);
        let hashStr2 = EncryptedMessage.decrypt(EncryptedMessage.createFromPayload(encryptedStr),this.globalPrivateKey,pubAccount);
        if (hashStr1 == hashStr2.payload){
            return true;
        }
        else return false;
    }

    addAuthOrigin(auth:string){
        this.auth_origin = auth;
    }

    addAuthOwner(auth:string){
        this.auth_owner = auth;
    }

    getContent(){
        return this.content;
    }

    getId(){
        return this.id;
    }

    getName(){
        return this.name;
    }

    getDescription(){
        return this.description;
    }

    getIcon(){
        return this.icon;
    }

    getDocumentHash(){
        return this.documentHash;
    }

    getAuthOrigin(){
        return this.auth_origin;
    }
    getAuthOwner(){
        return this.auth_owner;
    }
}