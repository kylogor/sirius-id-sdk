import { AppId } from './AppId';
import { Message } from './Message';
import {MessageType} from './MessageType';

export abstract class SiriusIdMessage extends Message {
    /**
     * App name to determine app message in transaction
     */
    public readonly appID = AppId.APP_NAME;

    constructor(type: MessageType, payload: object) {
        super(type, payload);
    }
}