import { CredentialRequestMessage } from '../src/message/CredentialRequestMessage';
import { MessageType } from '../src/message/MessageType';
import { Credentials } from '../src/message/Credentials';

/*Data from user input and dApp*/
const appId:string = undefined;

const credential = new Credentials(appId,null);
const credentialMessage = new CredentialRequestMessage(MessageType.CREDENTIAL_REQ,credential);

credentialMessage.generateQR();

