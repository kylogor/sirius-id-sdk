import { CredentialRequestMessage } from '../src/message/CredentialRequestMessage';
import { MessageType } from '../src/message/MessageType';
import { Credentials } from '../src/message/Credentials';

/*Data from user input and dApp*/
const appId:string = undefined;
const content: any = undefined;

const credential = new Credentials(appId,content);
const credentialMessage = new CredentialRequestMessage(MessageType.CREDENTIAL,credential);

credentialMessage.generateQR();

