import { MessageReceived } from '../src/service/MessageReceived';
import {Account, NetworkType, TransactionHttp, Listener} from 'tsjs-xpx-chain-sdk';

const transactionHttp = new TransactionHttp('http://localhost:3000');
const listener = new Listener('http://localhost:3000');

const sender = Account.createFromPrivateKey(
    '0123456789012345678901234567890123456789012345678901234567890123',
    NetworkType.MIJIN_TEST);

const generationHash = "3D9507C8038633C0EB2658704A5E7BC983E4327A99AC14D032D67F5AACBCCF6A";

let message = undefined; //receive from QRCode or deeplink
 
let messageReceived = new MessageReceived(message);
let tx = messageReceived.message.transaction;

let address = tx.recipient;
const signedTx = sender.sign(tx,generationHash);

listener.open().then(() => {
    const subscription = listener.confirmed(address).subscribe(confirmed => {
        if (confirmed && confirmed.transactionInfo && confirmed.transactionInfo.hash === signedTx.hash) {
            console.log('confirmed: ' + JSON.stringify(confirmed));
            subscription.unsubscribe();
            listener.close();
        }
    }, error => {
        console.error(error);
    }, () => {
        console.log('done.');
    });

    transactionHttp.announce(signedTx);
});
