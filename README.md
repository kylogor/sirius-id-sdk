# ProximaX SiriusID SDK for TypeScript and JavaScript

The official ProximaX SiriusID SDK for TypeScript and JavaScript, available for browsers, mobile applications and NodeJS, to work with the ProximaX Sirius Blockchain.

## Installing the library

Install using [npm](https://www.npmjs.org/):

```
    npm install siriusid-sdk
```

or using [yarn](https://yarnpkg.com/):
```
    yarn add siriusid-sdk
```

## Contributing

### Bootstrap your environments

1. Clone this repository.

```sh
git clone https://gitlab.com/proximax-enterprise/siriusid/sirius-id-sdk.git
```

2. Install the dependency packages.

```sh
npm install
```

### Build the packages

Build SDK

```sh
npm run build
```
or using below to build and increase version of SDK

```sh
npm run build-version
```

### Publish the packages
With the first time pushlish, you need log in first:

 ```sh
npm login --registry http://localhost:4873
 ```

 ```sh
npm publish --registry http://localhost:4873
 ```

Install [Verdaccio](https://verdaccio.org/docs/en/installation) as local registry

